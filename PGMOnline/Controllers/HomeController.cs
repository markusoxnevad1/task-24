﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PGMOnline.Models;

namespace PGMOnline.Controllers
{
    public class HomeController : Controller
    {
        
        public IActionResult Index()
        {
            ViewBag.WelcomeMessage = "Welcome";
            ViewBag.Time = DateTime.Now;
            return View("MySecondView");
        }
        public IActionResult SupervisorInfo()
        {
            List<Supervisor> supervisors = new List<Supervisor> {
                new Supervisor{Id=1, Name="Markus", FieldOfScience="Mathematics", YearsOfExperience=4},
                new Supervisor{Id=2, Name="John", FieldOfScience="Physical Education", YearsOfExperience=7},
                new Supervisor{Id=3, Name="Lisa", FieldOfScience="History", YearsOfExperience=8},
                new Supervisor{Id=4, Name="Snape", FieldOfScience="Defence Against the Dark Arts", YearsOfExperience=29}
            };
            return View(supervisors);
        }
    }
}
