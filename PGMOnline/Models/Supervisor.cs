﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PGMOnline.Models
{
    public class Supervisor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FieldOfScience { get; set; }
        public int YearsOfExperience { get; set; }
    }
}
